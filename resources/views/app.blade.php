<!DOCTYPE html>
<html lang="vi">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Trữ Tình Tôi Yêu</title>
	<!-- Fonts -->
	 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<header>
		<nav>
			<div class="nav-wrapper white">
				<div class="container">
					<a href="#!" class="brand-logo  grey-text text-darken-3">TrữTìnhTôiYêu.VN</a>
				<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a href="#" class="green-text">" Nhạc sĩ "</a></li>
					<li><a href="#" class="green-text"> " Ca sĩ "</a></li>
					<li><a href="#"class="grey-text">" Về Con " </a></li>
					<li><a href="#">Mobile</a></li>
				</ul>
				<ul class="side-nav" id="mobile-demo">
					<li><a href="#">Sass</a></li>
					<li><a href="#">Components</a></li>
					<li><a href="#">Javascript</a></li>
					<li><a href="#">Mobile</a></li>
				</ul>
				</div>
			</div>
		</nav>
	</header>
	<main>
		<div class="parallax-container valign-wrapper">
			<div class="parallax">				
				<img src="https://toiviettruyen.files.wordpress.com/2014/07/an-giang.jpg" alt="An Giang">
			</div>
			<div class="container valign">
 				<div class="row">
					<div class="col m4 s4 l3 ">
						 <img src="http://ecx.images-amazon.com/images/I/81VStYnDGrL.jpg" alt="" class="responsive-img z-depth-1"> <!-- notice the "circle" class -->
          </div>
					<div class="col m8 s12 l9 valign-wrapper">
						<div class="card valign">
							 <div class="card-title">
								Steve Jobs
								 <hr>
							</div>
							<div class="card-content">
									Steve Jobs (24 tháng 2 năm 1955 - 5 tháng 10, 2011) là doanh nhân và nhà sáng chế người Mỹ. Ông là đồng sáng lập viên, chủ tịch, và cựu tổng giám đốc điều hành của hãng Apple,[14][15] là một trong những người có ảnh hưởng lớn nhất ở ngành công nghiệp vi tính. Trước đây ông từng là tổng giám đốc điều hành của xưởng phim hoạt hình Pixar; sau đó trở thành thành viên trong ban giám đốc của công ty Walt Disney năm 2006, sau khi Disney mua lại Pixar. Ông được công nhận là điều hành sản xuất của bộ phim Toy Story (1995).
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	@yield('content')
	<!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
	<script>
		$(document).ready(function ()
		{
			$('.button-collapse').sideNav();	
				$('.parallax').parallax();
			var options = [
    
    {selector: '.card-title', offset:1, callback: 'Materialize.fadeInImage(".card-title")' }
  ];
  Materialize.scrollFire(options);
		});
	</script>
</body>
</html>
